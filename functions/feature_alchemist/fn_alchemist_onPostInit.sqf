private _players = [] call CBA_fnc_players;

{
   if ((isPlayer _x) && ((_x isKindOf "B_Officer_F") || (_x isKindOf "O_Officer_F"))) then {
    _x setVariable ["Alchemist",true,true];
};
} forEach _players;

if (isNil "lxWS_magicTime_value") then {
   lxWS_magicTime_value = 150;
   lxWS_regeneratePoint_Value = 3;
   lxWS_slowmoPrice_Value = 1.5;
   lxWS_teleportPrice_Value = 30;
   lxWS_teleportDistance_Value = 30;
   lxWS_RagePrice_Value = 75;
   lxWS_RageDistance_Value = 20;
   lxWS_HealPrice_Value = 200;
};

[] call lxWS_fnc_init_alchemistMagic; 
[] spawn lxWS_fnc_alchemistMagic;