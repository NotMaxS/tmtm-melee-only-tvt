// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{	
	#define UNIFORM_GENERAL "U_I_C_Soldier_Para_4_F", "U_I_C_Soldier_Para_3_F", "CUP_I_B_PARA_Unit_8", "U_I_E_Uniform_01_tanktop_F", "U_B_CTRG_Soldier_2_Arid_F", "U_lxWS_B_CombatUniform_desert_tshirt", "CUP_U_CRYE_TAN_Roll", "tmtm_u_guerillaGarment_rustSand"
	#define HEADGEAR_GENERAL "H_Simc_CVC_G_low", "lxWS_H_Tank_tan_F", "H_Simc_pasgt_m81_SWDG_low_b", "H_ShemagOpen_tan", "H_turban_02_mask_snake_lxws", "CUP_H_RUS_Altyn_Goggles_black", "H_turban_02_mask_black_lxws", "H_Simc_CVC_G", "H_Simc_CVC", "H_turban_02_mask_hex_lxws", "CUP_H_RUS_Ratnik_Balaclava_6M2_Desert_4"
	#define FACEWEAR_GENERAL"G_AirPurifyingRespirator_02_black_F", "G_AirPurifyingRespirator_02_sand_F", "G_AirPurifyingRespirator_02_olive_F"
	#define VEST_BLUE "tmtm_v_carrierVestLite_blue"
	#define VEST_RED "tmtm_v_carrierVestLite_red"

	#define STANDARD_MEDICAL {"ACE_quikclot",8},{"ACE_epinephrine",2},{"ACE_morphine",4},{"ACE_splint",1},{"ACE_tourniquet",2}

	class B_Soldier_F {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		displayName = "common Soldier Blue";

		primaryWeapon[] = {};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};

		uniformClass[] = {UNIFORM_GENERAL};
		headgearClass[] = {HEADGEAR_GENERAL};  
		facewearClass[] = {FACEWEAR_GENERAL}; 
		vestClass[] = {VEST_BLUE}; 

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch",""};


		uniformItems[] = {};
		vestItems[] = {};

		basicMedUniform[] = {STANDARD_MEDICAL};
		basicMedVest[] = {};
	};

	class O_Soldier_F {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		displayName = "common Soldier Blue";

		primaryWeapon[] = {};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};

		uniformClass[] = {UNIFORM_GENERAL};
		headgearClass[] = {HEADGEAR_GENERAL};  
		facewearClass[] = {FACEWEAR_GENERAL}; 
		vestClass[] = {VEST_RED}; 

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch",""};


		uniformItems[] = {};
		vestItems[] = {};

		basicMedUniform[] = {STANDARD_MEDICAL};

		basicMedVest[] = {};
	};

	class B_support_MG_F {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		displayName = "musketeer";

		primaryWeapon[] = {"sgun_HunterShotgun_01_F","","","",{"CUP_1Rnd_12Gauge_Slug",1},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"WBK_pipeStyledSword","","","",{},{},""};

		uniformClass[] = {UNIFORM_GENERAL};
		headgearClass[] = {HEADGEAR_GENERAL};  
		facewearClass[] = {FACEWEAR_GENERAL}; 
		vestClass[] = {VEST_BLUE}; 

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch",""};


		uniformItems[] = {{"CUP_1Rnd_12Gauge_Slug",12,1}};
		vestItems[] = {};

		basicMedUniform[] = {STANDARD_MEDICAL};

		basicMedVest[] = {};
	}; 

	class O_support_MG_F {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		displayName = "musketeer red";

		primaryWeapon[] = {"sgun_HunterShotgun_01_F","","","",{"CUP_1Rnd_12Gauge_Slug",1},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"WBK_pipeStyledSword","","","",{},{},""};

		uniformClass[] = {UNIFORM_GENERAL};
		headgearClass[] = {HEADGEAR_GENERAL};  
		facewearClass[] = {FACEWEAR_GENERAL}; 
		vestClass[] = {VEST_RED}; 

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch",""};


		uniformItems[] = {{"CUP_1Rnd_12Gauge_Slug",12,1}};
		vestItems[] = {};

		basicMedUniform[] = {STANDARD_MEDICAL};

		basicMedVest[] = {};
	};

	class B_Soldier_GL_F {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		displayName = "grenadier blue";

		primaryWeapon[] = {};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"WBK_axe","","","",{},{},""};

		uniformClass[] = {UNIFORM_GENERAL};
		headgearClass[] = {HEADGEAR_GENERAL};  
		facewearClass[] = {FACEWEAR_GENERAL}; 
		vestClass = "tmtm_v_carrierVestGL_blue";

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch",""};


		uniformItems[] = {};
		vestItems[] = {{"MiniGrenade",4,1}, {"SmokeShell",4,1}};

		basicMedUniform[] = {STANDARD_MEDICAL};

		basicMedVest[] = {};
	};

	class O_Soldier_GL_F {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		displayName = "grenadier red";

		primaryWeapon[] = {};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"WBK_axe","","","",{},{},""};

		uniformClass[] = {UNIFORM_GENERAL};
		headgearClass[] = {HEADGEAR_GENERAL};  
		facewearClass[] = {FACEWEAR_GENERAL}; 
		vestClass = "tmtm_v_carrierVestGL_red";

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch",""};


		uniformItems[] = {};
		vestItems[] = {{"MiniGrenade",6,1}, {"SmokeShell",4,1}};

		basicMedUniform[] = {STANDARD_MEDICAL};

		basicMedVest[] = {};
	};

	class B_officer_F {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		displayName = "mage blue";

		primaryWeapon[] = {};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"WBK_survival_weapon_4","","","",{},{},""};

		uniformClass[] = {UNIFORM_GENERAL};
		headgearClass[] = {HEADGEAR_GENERAL};  
		facewearClass[] = {FACEWEAR_GENERAL}; 
		vestClass[] = {VEST_BLUE}; 

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch",""};


		uniformItems[] = {};
		vestItems[] = {{"SmokeShell",6,1},{"ACE_CTS9",4,1},{"ACE_M14",4,1}};

		basicMedUniform[] = {STANDARD_MEDICAL};

		basicMedVest[] = {};
	};

	class O_officer_F {
		// Requires the following DLC:
		// Apex
		// Western Sahara
		displayName = "mage red";

		primaryWeapon[] = {};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"WBK_survival_weapon_4","","","",{},{},""};

		uniformClass[] = {UNIFORM_GENERAL};
		headgearClass[] = {HEADGEAR_GENERAL};  
		facewearClass[] = {FACEWEAR_GENERAL}; 
		vestClass[] = {VEST_RED}; 

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {{"SmokeShell",6,1},{"ACE_CTS9",4,1},{"ACE_M14",4,1}};

		basicMedUniform[] = {STANDARD_MEDICAL};

		basicMedVest[] = {};
	};
};