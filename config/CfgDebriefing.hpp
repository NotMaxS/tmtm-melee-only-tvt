// Mission endings
// Handles the mission ending screen
// https://community.bistudio.com/wiki/Debriefing

class blueLoss
{
	title = "Red Team Wins!"; // Main text that appears for the closing shot (ex: MISSION COMPLETED)
	subtitle = "Good effort to both teams."; // Subtitle below the title when the closing shot is triggered
	description = "Would you like to try again?";
};

class redLoss
{
	title = "Blue Team Wins!"; // Main text that appears for the closing shot (ex: MISSION COMPLETED)
	subtitle = "Good effort to both teams."; // Subtitle below the title when the closing shot is triggered
	description = "Would you like to try again?";
};